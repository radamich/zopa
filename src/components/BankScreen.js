// @flow

import React, { Component } from 'react';
import {
	View,
	Text,
	Image,
	Button,
	FlatList,
	TouchableOpacity,
} from 'react-native';
import PropTypes from 'prop-types';

import styles from '../styles/BankScreen.style';
import saltEdgeClient from '../utils/AxiosClient';
import ErrorView from '../components/ErrorView';
import { endpoints as SaltEdgeAPI } from '../config/SaltEdgeAPI.config';

const BANK_LOGOS: Array<string> = [
	require('../assets/Barclays.png'),
	require('../assets/LogoNatwest.png'),
	require('../assets/LogoLloyds.png'),
	require('../assets/LogoHSBC.png'),
	require('../assets/LogoTSB.png'),
	require('../assets/LogoSantander.png'),
];

export default class BankScreen extends Component {

	static propTypes = {
		navigation: PropTypes.object,
	}

	static navigationOptions = {
		title: 'Choose Bank',
		headerStyle: styles.navigationHeader,
		headerTintColor: 'white',
	}

	_params: { customer: Object };
	state: {
		providers: Array<Object>,
		selectedProvider: Object,
		error: string,
	};

	constructor(props: Object) {
		super(props);

		this._params = this.props.navigation.state.params;
		this.state = {
			providers: [],
			selectedProvider: null,
			error: null,
		};
	}

	componentDidMount() {
		this._getProviders();
	}

	_getProviders = () => {
		saltEdgeClient.get(SaltEdgeAPI.providers).then((response: Object) => {
			if (response.status === 200 && response.data) {
				this.setState({
					providers: response.data.data.slice(0, 6),
				});
			}
			console.log('response: ', response);
		}).catch((err: Object) => {
			console.log('error: ', JSON.stringify(err));
			const error = err.response ? err.response.data.error_message : 'Unable to fetch Bank information.';
			this.setState({ error });
		});
	}

	_selectProvider = (provider: Object) => {
		return () => {
			this.setState({
				providers: [...this.state.providers],
				selectedProvider: provider,
			});
		};
	}

	_renderProvider = ({ item, index }: { item: Object, index: number }) => {
		const selectedProvider = this.state.selectedProvider;
		return (
			<View
				style={[
					styles.provider,
					selectedProvider && selectedProvider.id === item.id ? { borderColor: 'black' } : null,
				]}
			>
				<TouchableOpacity
					onPress={this._selectProvider(item)}
					style={styles.bankLogoTouchable}
				>
					<Image
						source={BANK_LOGOS[index]}
						resizeMode={'contain'}
						style={styles.bankLogoImage}
					/>
				</TouchableOpacity>
			</View>
		);
	}

	_keyExtractor = (item: Object): number => {
		return item.code;
	}

	_renderContinueButton() {
		if (this.state.providers.length === 0) {
			return null;
		}
		return (
			<View style={styles.continueButton}>
				<Button
					title={'Continue'}
					onPress={() => { this.props.navigation.navigate('LoginScreen', { provider: this.state.selectedProvider, customer: this._params.customer }); }}
					color={'coral'}
					disabled={!this.state.selectedProvider}
				/>
			</View>
		);
	}

	_renderProviderList() {
		if (this.state.error) {
			return (
				<ErrorView
					message={this.state.error}
					onRetry={() => { this.setState({ error: null }, this._getProviders); }}
				/>
			);
		} else if (this.state.providers.length === 0) {
			return <Text style={styles.loadingText}>Loading Banks...</Text>;
		}
		return (
			<FlatList
				data={this.state.providers}
				renderItem={this._renderProvider}
				numColumns={2}
				keyExtractor={this._keyExtractor}
				columnWrapperStyle={styles.providerListColumn}
			/>
		);
	}

	render() {
		return (
			<View style={styles.container}>
				<Text style={styles.headerText}>
					Which bank does this account belong to?
				</Text>
				<Text style={styles.bodyText}>
					From the list below choose the bank you want to connect your account from.
				</Text>
				{ this._renderProviderList() }
				{ this._renderContinueButton() }
			</View>
		);
	}
}
