// @flow

import React, { Component } from 'react';
import {
	View,
	Text,
	SectionList,
	Picker,
} from 'react-native';
import PropTypes from 'prop-types';
import moment from 'moment';

import styles from '../styles/TransactionScreen.style';
import saltEdgeClient from '../utils/AxiosClient';
import ErrorView from '../components/ErrorView';
import { endpoints as SaltEdgeAPI } from '../config/SaltEdgeAPI.config';

const SECTION_HEADER_DATE_FORMAT = 'DD MMMM YYYY';

export default class TransactionScreen extends Component {

	static propTypes = {
		navigation: PropTypes.object,
	}

	static navigationOptions = {
		title: 'Your transactions',
		headerStyle: styles.navigationHeader,
		headerTintColor: 'white',
	}

	_params: { loginId: number, provider: Object };
	_accounts: Array<Object>;
	_dataSource: Object;
	_transactions: Array<Object>;
	state: {
		transactions: Array<Object>,
		accountId: number,
		error: string,
	};

	constructor(props: Object) {
		super(props);

		this._dataSource = {};
		this._params = this.props.navigation.state.params;
		this.state = {
			error: null,
		};
	}

	componentDidMount() {
		// Transactions (fake) are not available immediately after login
		// it takes over 10 seconds until they're available
		setTimeout(() => {
			this._getTransactions();
			this._getClientAccounts();
			console.log('TIMED OUT');
		}, 13000);
	}

	componentWillUnmount() {
		// TODO: Log out needs to be moved to separate section
		// with proper error handling
		this._logout();
	}

	_logout() {
		saltEdgeClient.delete(`${SaltEdgeAPI.login}/${this._params.loginId}`).then((response: Object) => {
			if (response === 200) {
				console.log('Succesfully logged out');
				// other actions...
			}
		}).catch((err: Object) => {
			console.log('Unable to log out', err.response);
			// handle error...
		});
	}

	_getTransactions() {
		saltEdgeClient.get(SaltEdgeAPI.transactions, { params: { login_id: this._params.loginId } }).then((response: Object) => {
			if (response.status === 200) {
				this._transactions = response.data.data;
				console.log('_getTransactions: ', response.data.data.length);
				this.setState({
					transactions: this._formatDataBeforeRender(this._transactions),
				});
			}
		}).catch((err: Object) => {
			console.log('Error fetching transactions: ', err.response);
			const errorMessage: string = err.response ? err.response.data.error_message : 'Unable to fetch transactions.';
			this.setState({ error: errorMessage });
		});
	}

	_getClientAccounts = () => {
		saltEdgeClient.get(SaltEdgeAPI.accounts, { params: { login_id: this._params.loginId } }).then((response: Object) => {
			if (response.status === 200) {
				this._accounts = response.data.data;
				console.log('_getClientAccounts: ', response.data.data.length);
				this.setState({
					accountId: 0,
				});
			}
		}).catch((err: Object) => {
			console.log('Error fetching accounts: ', err.response);
			const errorMessage: string = err.response ? err.response.data.error_message : 'Unable to fetch account information.';
			this.setState({ error: errorMessage });
		});
	}

	_formatDataBeforeRender = (transactionData: Array<Object>, accountId: ?number): Array<Object> => {
		if (!transactionData || transactionData.length === 0) {
			return [{ data: [], title: 'No trasactions' }];
		}

		transactionData.forEach((transaction: Object) => {
			const sectionTitle: string = moment(transaction.made_on).format(SECTION_HEADER_DATE_FORMAT);
			if (this._dataSource[transaction.made_on]) {
				this._dataSource[transaction.made_on].data.push(transaction);
				if (this._dataSource[transaction.made_on].accounts.indexOf(transaction.account_id) === -1) {
					this._dataSource[transaction.made_on].accounts.push(transaction.account_id);
				}
			} else {
				this._dataSource[transaction.made_on] = {
					title: sectionTitle,
					data: [transaction],
					key: sectionTitle,
					accounts: [transaction.account_id],
				};
			}
		});

		return Object.keys(this._dataSource).map((sectionTitle) => {
			return this._dataSource[sectionTitle];
		});
	}

	_selectAccount = (accountId: number) => {
		if (this.state && this.state.accountId === accountId) {
			return;
		}

		this.setState({
			accountId,
		});
	}

	_renderAccountPicker = () => {
		const selectedAccount: number|string = (this.state && this.state.accountId) || 'All';
		const accounts: Array<Object> = this._accounts || [];
		return (
			<Picker
				selectedValue={selectedAccount}
				onValueChange={this._selectAccount}
				style={styles.accountPicker}
			>
				<Picker.Item
					key={0}
					label={'All'}
					value={0}
				/>
				{
					accounts.map((account: Object) => {
						return (
							<Picker.Item
								key={account.id}
								label={account.name}
								value={account.id}
							/>
						);
					})
				}
			</Picker>
		);
	}

	_renderSectionHeader = ({ section }: Object) => {
		if (this.state && this.state.accountId && section && section.accounts.indexOf(this.state.accountId) === -1) {
			return null;
		}
		return (
			<View style={styles.sectionHeader}>
				<Text style={styles.sectionHeaderText}>{ section.title }</Text>
			</View>
		);
	}

	formatedAmount(transaction: Object): string {
		const currencySymbol: Object = {
			USD: '$',
			EUR: '€',
			GBP: '£',
		};
		const amount: number = transaction.amount.toFixed(2).replace(/./g, (c, i, a) => {
			return i && c !== '.' && ((a.length - i) % 3 === 0) ? `,${c}` : c;
		});
		const currency: string = currencySymbol[transaction.currency_code] || transaction.currency_code;
		if (amount < 0) {
			return `- ${currency}${Math.abs(amount)}`;
		}
		return currency + amount;
	}

	_renderTransaction = ({ item }: Object) => {
		if (item.account_id === this.state.accountId || this.state.accountId === 0) {
			return (
				<View style={styles.transaction}>
					<Text style={styles.transactionTextLeft}>{ item.description }</Text>
					<Text style={styles.transactionTextRight}> { this.formatedAmount(item) }</Text>
				</View>
			);
		}

		return null;
	}

	_keyExtractor = (transaction: Object): number => {
		return transaction.id;
	}

	_itemSeparator() {
		return <View style={styles.itemSeparator} />;
	}

	_renderTransactionList = () => {
		if (this.state.error) {
			return <ErrorView message={this.state.error} />;
		} else if (!this.state || !this.state.transactions || this.state.transactions.length === 0) {
			return <Text style={styles.loadingText}>Loading transactions...</Text>;
		}
		return (
			<SectionList
				renderItem={this._renderTransaction}
				sections={this.state.transactions}
				renderSectionHeader={this._renderSectionHeader}
				ItemSeparatorComponent={this._itemSeparator}
				keyExtractor={this._keyExtractor}
				style={{ flex: 1 }}
				initialNumToRender={15}
			/>
		);
	}

	render() {
		return (
			<View style={styles.container}>
				<View style={styles.accountInfo}>
					<View style={styles.clientName}>
						<Text style={styles.bankName}>{ this._params.provider.name }</Text>
						<Text style={styles.bankName}> DOE. J. </Text>
					</View>
					<View style={styles.accountNumber}>
						<Text style={styles.bankName}>CURRENT ACCOUNT</Text>
						{ this._renderAccountPicker() }
					</View>
				</View>
				<View>
					<Text style={styles.transactionListHeader}>
						Your transactions for the last 30 days
					</Text>
				</View>
				{ this._renderTransactionList() }
			</View>
		);
	}
}
