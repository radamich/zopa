// @flow

import React from 'react';
import {
    Button,
    StyleSheet,
    Text,
    View,
} from 'react-native';
import PropTypes from 'prop-types';

function _renderRetryButton(callback: Function) {
	if (!callback) {
		return null;
	}

	return (
		<View style={styles.button}>
			<Button
				title={'Retry'}
				onPress={callback}
			/>
		</View>
	);
}


export default function ErrorMessage(props: Object) {
	if (!props || !props.message) {
		return null;
	}

	return (
		<View style={styles.error}>
			<Text style={styles.errorText}>ERROR: { props.message }</Text>
			{ _renderRetryButton(props.onRetry) }
		</View>
	);
}


ErrorMessage.propTypes = {
	message: PropTypes.string,
	onRetry: PropTypes.func,
};

const styles: StyleSheet = StyleSheet.create({
	error: {
		margin: 5,
		backgroundColor: 'crimson',
		padding: 5,
		borderRadius: 2,
		width: '95%',
	},
	errorText: {
		fontSize: 15,
		fontWeight: 'bold',
		color: 'white',
		textAlign: 'center',
	},
	button: {
		marginTop: 5,
		alignItems: 'center',
	},
});
