// @flow

import React, { Component } from 'react';
import {
	Button,
	View,
	Text,
	TextInput,
	Picker,
} from 'react-native';
import PropTypes from 'prop-types';

import styles from '../styles/LoginScreen.style';
import saltEdgeClient from '../utils/AxiosClient';
import ErrorView from '../components/ErrorView';
import { endpoints as SaltEdgeAPI } from '../config/SaltEdgeAPI.config';

const INPUT_TYPE = { PASSWORD: 'password', SELECT: 'select' };
const LOGIN_ERRORS: Object = {
	DUPLICATED: 'LoginDuplicated',
};

export default class LoginScreen extends Component {

	static propTypes = {
		navigation: PropTypes.object,
		styles: PropTypes.object,
	}

	static navigationOptions = {
		title: 'Connect to bank account',
		headerStyle: styles.navigationHeader,
		headerTintColor: 'white',
	}

	_params: { provider: Object, customer: Object };
	_loginFields: Object;
	_credentials: Object;
	state: {
		error: string,
	}
	constructor(props: Object) {
		super(props);

		this._params = this.props.navigation.state.params;
		this._loginFields = {};
		this.state = {
			error: null,
		};
	}

	get _loginObject(): Object {
		return {
			data: {
				customer_id: this._params.customer.id,
				country_code: 'XF',
				provider_code: this._params.provider.code,
				credentials: this._loginFields,
			},
		};
	}

	_setLoginFields = (input: Object) => {
		Object.assign(this._loginFields, input);
	}

	_getPickerDefaultField = (pickerInput: Object) => {
		if (!pickerInput || !pickerInput.field_options || pickerInput.field_options.length === 0) {
			return { option_value: '' };
		}
		let defaultField: Object = pickerInput.field_options.find((optionField) => {
			return optionField.selected === true;
		});
		defaultField = defaultField || pickerInput.field_options[0];
		this._setLoginFields({ [pickerInput.name]: defaultField.option_value });

		return defaultField;
	}

	_renderPickerInput = (inputField: Object, key: number) => {
		const selectedValue: string = (this.state && this.state[inputField.name]) || this._getPickerDefaultField(inputField).option_value;
		return (
			<Picker
				key={key}
				selectedValue={selectedValue}
				onValueChange={(optionValue: any, optionIndex: number) => {
					this._setLoginFields({ [inputField.name]: optionValue });
					this.setState({ [inputField.name]: inputField.field_options[optionIndex].name });
				}}
				style={styles.pickerText}
			>
				{
							inputField.field_options.map((option: Object, optionIndex: number) => {
								return (
									<Picker.Item
										key={optionIndex}
										label={option.english_name}
										value={option.option_value}
									/>
								);
							})
						}
			</Picker>
		);
	}

	_renderInputs = () => {
		if (!this._params || !this._params.provider || !this._params.provider.required_fields) {
			return null;
		}

		return this._params.provider.required_fields.map((inputField: Object, index: number) => {
			if (inputField.nature === INPUT_TYPE.SELECT || inputField.field_options) {
				return this._renderPickerInput(inputField, index);
			}

			return (
				<TextInput
					key={index}
					autoFocus={index === 0}
					placeholder={inputField.english_name}
					secureTextEntry={inputField.name === INPUT_TYPE.PASSWORD}
					placeholderTextColor={'rgba(255, 255, 255, 0.8)'}
					style={styles.input}
					underlineColorAndroid={'white'}
					onChangeText={(inputValue) => { this._setLoginFields({ [inputField.name]: inputValue }); }}
				/>
			);
		});
	}

	_handleLogingError(error: Object) {
		let errorMessage: string;
		if (error.response) {
			if (error.response.data.error_class === LOGIN_ERRORS.DUPLICATED) {
				const loginId = error.response.data.error_message.replace(/[^0-9]*'([0-9]*)'.*$/, '$1');
				this.props.navigation.navigate('TransactionScreen', { loginId, provider: this._params.provider });
				errorMessage = null;
			} else {
				errorMessage = error.response.data.error_message;
			}
		} else {
			errorMessage = 'Unable to login';
		}
		this.setState({ error: errorMessage });
	}

	_performLogin = () => {
		console.log('Login fields: ', this._loginFields);
		saltEdgeClient.post(SaltEdgeAPI.login, this._loginObject).then((response: Object) => {
			if (response.status === 200) {
				this.state.error && this.setState({ error: null });
				console.log('LOGIN ID: ', response.data.data.id);
				this.props.navigation.navigate('TransactionScreen', { loginId: response.data.data.id, provider: this._params.provider });
			}
		}).catch((err: Object) => {
			console.log('Unable to log in: ', err);
			this._handleLogingError(err);
		});
	}

	render() {
		return (
			<View style={styles.container}>
				<Text style={styles.headerText}>
					Log in to your online banking
				</Text>
				<Text style={styles.bodyText}>
					Enter the same details you use to login to your online banking.
				</Text>
				<ErrorView message={this.state.error} />
				<View style={styles.inputsContainer}>
					{ this._renderInputs() }
				</View>
				<View style={styles.loginButton}>
					<Button
						title={'Log in'}
						onPress={this._performLogin}
						color={'coral'}
					/>
				</View>
			</View>
		);
	}
}
