// @flow

import React, { Component } from 'react';
import {
	AsyncStorage,
	Button,
	Text,
	View,
	Image,
} from 'react-native';
import { StackNavigator } from 'react-navigation';

import styles from '../styles/App.style';
import BankScreen from '../components/BankScreen';
import LoginScreen from '../components/LoginScreen';
import TransactionScreen from '../components/TransactionScreen';
import ErrorView from '../components/ErrorView';
import saltEdgeClient from '../utils/AxiosClient';
import SaltEdgeConfig, { endpoints as SaltEdgeAPI } from '../config/SaltEdgeAPI.config';

const CUSTOMER_ASYN_STORAGE_KEY = 'customer';
const NOPA_LOGO = require('../assets/LogoNopa.png');

class HomeScreen extends Component {

	static navigationOptions = {
		// title: 'NOPA',
		headerVisible: false,
		headerStyle: styles.navigationHeader,
		headerTintColor: 'white',
	}

	state: {
		customer: Object,
		error: string,
	}

	constructor(props: Object) {
		super(props);

		this.state = {
			customer: null,
			error: null,
		};
	}

	componentDidMount() {
		this._fetchCustomer();
	}

	_getCustomer = () => {
		AsyncStorage.getItem(CUSTOMER_ASYN_STORAGE_KEY).then((customer: string) => {
			if (customer === null) {
				this._fetchCustomer();
			} else {
				this.setState({ customer: JSON.parse(customer) });
			}
		}).catch((err: Object) => {
			this._fetchCustomer();
		});
	}

	_fetchCustomer() {
		saltEdgeClient.get(`${SaltEdgeAPI.customers}/${SaltEdgeConfig.customerID}`).then((response: Object) => {
			AsyncStorage.setItem(CUSTOMER_ASYN_STORAGE_KEY, JSON.stringify(response.data.data));
			this.setState({ customer: response.data.data });
		}).catch((err: Object) => {
			console.log('errr', err);
			this.setState({ error: 'Unable to fetch customer ID.' });
		});

		// saltEdgeClient.post(SaltEdgeAPI.customers, { data: { identifier: 'michal1' } }).then((response: Object) => {
		// 	if (response.status === 200) {
		// 		this.setState({ customer: response.data.data });
		// 		AsyncStorage.setItem(CUSTOMER_ASYN_STORAGE_KEY, JSON.stringify(response.data.data));
		// 	}
		// }).catch((err: Object) => {
		// 	console.log('error fetching customer: ', err.response.data);
		// });
	}

	render() {
		const { navigate }: { navigate: Function } = this.props.navigation;

		return (
			<View style={styles.root}>
				<ErrorView
					message={this.state.error}
					onRetry={() => { this.setState({ error: null }, this._getCustomer); }}
				/>
				<View style={styles.container}>
					<Image
						source={NOPA_LOGO}
						style={styles.imageNopa}
						resizeMode={'contain'}
					/>
					<Text style={styles.headerText}>
						Welcome to Zopa connect
					</Text>
					<Text style={styles.bodyText}> Keep track of all your expenses by connecting as many bank accounts as you’d like to your Zopa app </Text>
				</View>
				<View style={styles.button}>
					<Button
						disabled={this.state.customer === null}
						title={'Select your fake bank accout'}
						color={'coral'}
						onPress={() => { navigate('BankScreen', { customer: this.state.customer }); }}
						//  onPress={this._getProviders}
					/>
				</View>
			</View>
		);
	}
}

export default StackNavigator({
	Home: { screen: HomeScreen },
	BankScreen: { screen: BankScreen },
	LoginScreen: { screen: LoginScreen },
	TransactionScreen: { screen: TransactionScreen },
});
