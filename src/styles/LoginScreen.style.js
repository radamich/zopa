import { StyleSheet } from 'react-native';

export default StyleSheet.create({
	root: {
		flex: 1,
	},
	navigationHeader: {
		borderWidth: 0,
		backgroundColor: '#304FFE',
	},
	container: {
		backgroundColor: '#304FFE',
		flex: 1,
		alignItems: 'center',
		paddingTop: 10,
		// justifyContent: 'center',
	},
	headerText: {
		fontFamily: 'Montserrat',
		fontSize: 25,
		fontWeight: '500',
		color: 'white',
		width: '80%',
		textAlign: 'center',
	},
	bodyText: {
		fontSize: 14,
		fontWeight: 'bold',
		color: 'white',
		textAlign: 'center',
		marginBottom: 20,
		marginTop: 10,
	},
	pickerText: {
		color: 'rgba(255, 255, 255, 0.8)',
	},
	input: {
		color: 'white',
		opacity: 0.8,
	},
	inputsContainer: {
		flex: 1,
		width: '100%',
		padding: 10,
	},
	loginButton: {
		padding: 10,
		width: '100%',
	},
})
;
