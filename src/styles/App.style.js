import { StyleSheet } from 'react-native';

export default StyleSheet.create({
	root: {
		flex: 1,
	},
	navigationHeader: {
		borderWidth: 0,
		backgroundColor: '#304FFE',
	},
	container: {
		backgroundColor: '#304FFE',
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
		padding: 10,
	},
	imageNopa: {
		width: '30%',
		maxWidth: 100,
	},
	headerText: {
		fontSize: 25,
		fontWeight: 'bold',
		color: 'white',
		width: '80%',
		textAlign: 'center',
	},
	bodyText: {
		fontSize: 14,
		fontWeight: 'bold',
		color: 'white',
		textAlign: 'center',
	},
	button: {
		backgroundColor: '#304FFE',
		padding: 10,
	},
});
