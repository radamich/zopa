import { StyleSheet } from 'react-native';

export default StyleSheet.create({
	navigationHeader: {
		borderWidth: 0,
		backgroundColor: '#304FFE',
	},
	container: {
		backgroundColor: '#304FFE',
		flex: 1,
		alignItems: 'center',
		// justifyContent: 'center',
	},
	headerText: {
		fontFamily: 'Montserrat',
		fontSize: 25,
		fontWeight: '500',
		color: 'white',
		width: '80%',
		textAlign: 'center',
	},
	bodyText: {
		fontSize: 14,
		fontWeight: 'bold',
		color: 'white',
		textAlign: 'center',
		marginBottom: 20,
		padding: 5,
	},
	loadingText: {
		fontSize: 15,
		fontWeight: 'bold',
		color: 'white',
		textAlign: 'center',
	},
	button: {
		backgroundColor: '#304FFE',
		padding: 10,
	},
	providerList: {
		flexDirection: 'row',
		flex: 1,
		flexWrap: 'wrap',
	},
	providerListColumn: {
		justifyContent: 'center',
	},
	provider: {
		backgroundColor: 'white',
		margin: 5,
		height: 100,
		width: '45%',
		borderWidth: 5,
		borderColor: 'transparent',
	},
	bankLogoImage: {
		width: '90%',
		height: '50%',
	},
	bankLogoTouchable: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
	},
	continueButton: {
		width: '100%',
		padding: 10,
	},
})
;
