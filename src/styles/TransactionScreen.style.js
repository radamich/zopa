import { StyleSheet } from 'react-native';

export default StyleSheet.create({
	navigationHeader: {
		borderWidth: 0,
		backgroundColor: '#304FFE',
	},
	container: {
		backgroundColor: '#304FFE',
		flex: 1,
	},
	accountInfo: {
		flexDirection: 'row',
		marginTop: 15,
		marginBottom: 15,
		paddingLeft: 10,
		paddingRight: 10,
	},
	bankName: {
		fontSize: 15,
		fontWeight: 'bold',
		color: 'white',
	},
	clientName: {
		flex: 1,
	},
	accountNumber: {
		flex: 1,
	},
	accountPicker: {
		color: 'white',
	},
	loadingText: {
		fontSize: 15,
		fontWeight: 'bold',
		color: 'white',
		textAlign: 'center',
	},
	transactionListHeader: {
		padding: 10,
		color: 'white',
		fontWeight: 'bold',
	},
	sectionHeader: {
		padding: 10,
		backgroundColor: 'lightgray',
	},
	sectionText: {
		fontSize: 14,
		color: 'black',
	},
	transaction: {
		flexDirection: 'row',
		flex: 1,
		padding: 10,
		backgroundColor: 'white',
	},
	itemSeparator: {
		borderTopColor: 'lightgray',
		borderTopWidth: 1,
	},
	transactionTextLeft: {
		flex: 2,
	},
	transactionTextRight: {
		flex: 1,
		textAlign: 'right',
	},
	error: {
		margin: 10,
		backgroundColor: 'crimson',
		padding: 5,
	},
	errorText: {
		fontSize: 15,
		fontWeight: 'bold',
		color: 'white',
		textAlign: 'center',
	},
})
;
