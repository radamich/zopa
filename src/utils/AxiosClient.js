import axios from 'axios';
import saltEdgeAPIConfig from '../config/SaltEdgeAPI.config';

const saltEdgeClient = axios.create({
	baseURL: 'https://www.saltedge.com/api/v3',
	timeout: 10000,
	headers: {
		'Client-id': saltEdgeAPIConfig.clientID,
		'Service-secret': saltEdgeAPIConfig.serviceSecret,
	},
})
;

export default saltEdgeClient;
